﻿using System;
using System.Windows.Forms;

namespace Programming_Learning_Environment
{
    /// <summary>Class that creates instance of designed Windows Form user interface, and defines the handling of events thrown by relevant form contols</summary>
    /// <remarks>Partial class, partners IDE generated code. <see cref="Programming_Window.InitializeComponent">Generated Code</see>
    /// <para>Inherits from System.Windows.Forms.Form</para></remarks>

    public partial class Programming_Window : Form
    {
        /// <summary>Referance pointer to object responcible for command parser</summary>
        private Interpreter draw;
        /// <summary>Referance pointer to object responcible for file output operations</summary>
        private SaveFileDialog save;
        /// <summary>Referance pointer to object responcible for file input operations</summary>
        private OpenFileDialog open;

        /// <summary>Constructor:<br/>
        ///Initialises display output source to 'Display' picturebox graphics context.<br/>
        ///Initialises I/O objects and filename/type filters</summary>
        public Programming_Window()
        {
            InitializeComponent();
            draw = Factory.makeInterpreter( Display.CreateGraphics() );
            save = Factory.makeSaveFileDialog();
            open = Factory.makeOpenFileDialog();

            save.Filter = "Text files (*.txt)|*.txt";
            open.Filter = "Text files (*.txt)|*.txt";
        }


        /// <summary>Method for displaying messages to the user - primarily the output of command and sysntax error checks - as a message box</summary>
        /// <param name="messageIn">Content to be displayed to message box body</param>
        /// <param name="messageType">Title of message box</param>
        public void Message(string messageIn, string messageType)
        {
            MessageBox.Show(messageIn, messageType);
        }

        /// <summary>Handling code in the event user clicks to type in the command line.<br/>
        /// Clears command line textbox of text</summary>
        /// <param name="sender">Control that triggered the event</param>
        /// <param name="e">Refereances event data</param>
        private void CmdLine_Click(object sender, EventArgs e)
        {
            this.Command_Line.Text = "";
        }

        /// <summary>Handling code in the event user clicks elsewhere, shifting event focus away form the command line<br/>
        /// Reasserts default text box content - [ENTER COMMAND]</summary>
        /// <param name="sender">Control that triggered the event</param>
        /// <param name="e">Refereances event data</param>
        private void CmdLine_IfBlank(object sender, EventArgs e)
        {
            if ( this.Command_Line.Text.Equals("") )
            {
                this.Command_Line.Text = "[enter command]";
            }
        }

        /// <summary>Handling code in the event user enters a command in the command line text box<br/>
        /// Is the primary contol mechanism for the end user. Command and Syntax checking is automatic and mandatory</summary>
        /// <remarks>Direct commands:<br/>
        /// RUN = execute lines in the program code text box<br/>
        /// SAVE = output contents of program code text box to a .txt file<br/>
        /// LOAD = input content of a .txt file to the progam code text box<br/><br/>
        /// Indirect commands = entering valid program code, line-by-line</remarks>
        /// <param name="sender">Control that triggered the event</param>
        /// <param name="e">Refereances event data</param>
        private void CmdLine_IfEnter(object sender, KeyEventArgs e)
        {
            //Function 1 - Running single lines of code via command line
            if ( e.KeyCode == Keys.Enter && !this.Command_Line.Text.Equals("") && !this.Command_Line.Text.Equals("RUN") && !this.Command_Line.Text.Equals("SAVE") && !this.Command_Line.Text.Equals("LOAD"))
            {
                string lineIn = this.Command_Line.Text.ToLower();
                string errorCode = draw.CheckCommandLine(lineIn); // Mandatory input validation

                //Displays error message in message box, unless returned error string is "ignore"
                if (!errorCode.Equals("ignore"))
                {
                    Message(errorCode, "Command Line Error");
                }
                else
                {
                    errorCode = draw.InterpretLine(lineIn);

                    if (!errorCode.Equals("ignore"))
                    {
                        Message(errorCode, "Run-time Error");
                    }
                    else
                    {
                        this.Command_Line.Text = "";
                    }
                }
            }
            //Function 2 - Running multiple lines of code, from code area
            else if ( e.KeyCode == Keys.Enter && this.Command_Line.Text.Equals("RUN") )
            {
                string errorCode;
                Boolean canRun = true; //Line can be run until proven erroneous
                string codeIn = Program_Code.Text.ToLower();
                string[] checkCodeIn = codeIn.Split( Factory.makeCharacterArray( '\n' ) );

                //For each line in code, displays error message in message box, unless returned error string is "ignore"
                foreach (string line in checkCodeIn)
                {
                    errorCode = draw.CheckCommandLine(line);

                    if (!errorCode.Equals("ignore"))
                    {
                        Message(errorCode, "Program Line Error");
                        canRun = false;
                        break;
                    }
                }
                // No code will be executed until the whole code is error-free
                if (canRun)
                {
                    errorCode = draw.InterpretCode(codeIn);

                    if (!errorCode.Equals("ignore"))
                    {
                        Message(errorCode, "Run-time Error");
                    }
                    else
                    {
                        this.Command_Line.Text = "";
                    }
                }
            }
            //Function 3.1 - Save code content
            else if (e.KeyCode == Keys.Enter && this.Command_Line.Text.Equals("SAVE"))
            {
                if ( save.ShowDialog() == DialogResult.OK )
                {
                    System.IO.File.WriteAllText( save.FileName, this.Program_Code.Text );
                }
            }
            //Function 3.2 - Load code content
            else if (e.KeyCode == Keys.Enter && this.Command_Line.Text.Equals("LOAD"))
            {
                if (open.ShowDialog() == DialogResult.OK)
                {
                    this.Program_Code.Text = System.IO.File.ReadAllText( open.FileName );
                }
            }

        }
    }
}
