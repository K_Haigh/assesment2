﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programming_Learning_Environment
{
    interface I_Draw
    {
        //Basic pen controls
        void MoveTo(int x_ord, int y_ord);
        void DrawTo(int x_ord, int y_ord);
        //Advanced pen controls
        void SetColour(Color colour);
        void SetFill(Boolean toFill);

        //Basic shapes
        void Circle(int radius);
        void Triangle(int a_side, int b_side);
        void Rectangle(int a_side, int b_side);
        //Advanced shape
        void Polygon(int points, List<KeyValuePair<int, int>> coordinates);

        //Basic canvas controls
        void Clear();
        void Reset();

    }
}
