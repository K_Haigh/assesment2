﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programming_Learning_Environment
{
    /// <summary>Abstract class defining the methods for the parsing of user entered 'code' into C#</summary>
    interface I_Interpreter
    {
        /// <summary>Method for the parsing of a single line of 'code' into implementation C# code<br/><br></br>
        /// Parsing should comprise of splitting a line into the command word and any accompanying data. The data should then be parsed into individual values.<br/>
        /// C# implementation code should reflect the parsed command and enact a function on or with the supplied values.</summary>
        /// <remarks>Returns a string dependant upon parsing success; containing either a success phrase or details about a run-time error.</remarks>
        /// <param name="lineIn">String of a single line of 'code' to be parsed into C#</param>
        /// <returns>Return code expressed as a string</returns>
        string InterpretLine(String lineIn);
        /// <summary>Method attempts to parse a single line of 'code' without parsing it to implementation C# code<br/>
        /// This method checks that the command can be parsed, but does implement any action.</summary>
        /// <remarks>Returns a string dependant upon parsing success; containing either a success phrase or details about a 'compile-time' error.</remarks>
        /// <param name="lineIn">String of a single line of 'code' to be checked for command parsability</param>
        /// <returns>Return code expressed as a string</returns>
        string CheckCommandLine(String lineIn);
        /// <summary>Method attempts to parse data passed into it from a parsed line of 'code' without passing it to C# implementation code<br/>
        /// Method checks that the data can be parsed, but does not process it with any logic</summary>
        /// <remarks>Returns a string dependant upon parsing success; containing either a success phrase or details about a 'compile-time' error.</remarks>
        /// <param name="lineIn">String of a single line of 'code' to be checked for data parsability</param>
        /// <returns>Return code expressed as a string</returns>
        string CheckCommandParameters(String lineIn);

        //Implement in Part 2
        /// <summary>Method for parsing a line of 'code' intended to declare a whole number variable into implementation C# code<br/><br/>
        /// Parsing should comprise of splitting a line into the variable name and a single integer value.<br/>
        /// C# implementation should reflect the variable name, datatype and value.</summary>
        /// <remarks>Returns a string dependant upon parsing success; containing either a success phrase or details about a ?AN ERROR?.</remarks>
        /// <param name="variableName">String containing user defined variable name</param>
        /// <param name="value">Integer value supplied by user</param>
        /// <returns>Return code expressed as a string</returns>
        string DeclareVariable(string variableName, int value);
        /// <summary>Method for parsing a line of 'code' intended to declare a worded variable into implementation C# code<br/><br/>
        /// Parsing should comprise of splitting a line into the variable name and a single string value.<br/>
        /// C# implementation should reflect the variable name, datatype and value.</summary>
        /// <remarks>Returns a string dependant upon parsing success; containing either a success phrase or details about a ?AN ERROR?.</remarks>
        /// <param name="variableName">String containing user defined variable name</param>
        /// <param name="value">String supplied by user</param>
        /// <returns>Return code expressed as a string</returns>
        string DeclareVariable(string variableName, string value);
        /// <summary>Method for parsing lines of 'code' intended to declare a method into implementation C# code<br/><br/>
        /// Parsing should comprise of splitting the first line into the method name and a list of parameters. Subsequent lines should be parsed to individual commands, until the end keyword line<br/>
        /// C# implementation should reflect the method name, accepted arguments and the 'code' lines the method describes.</summary>
        /// <remarks>Returns a string dependant upon parsing success; containing either a success phrase or details about ?AN ERROR?.</remarks>
        /// <param name="methodName">String containing user defined method name</param>
        /// <param name="methodParameters">String array containing user defined method parameters</param>
        /// <param name="methodLines">String array contiaing user defined method logic/code</param>
        /// <returns>Return code expressed as a string</returns>
        string DeclareMethod(string methodName, string[] methodParameters, string[] methodLines);
        /// <summary>Method for calling a user defined method, should that method exist, and actioning it in C#</summary>
        /// <remarks>Returns a string dependant upon parsing success; containing either a success phrase or details about ?AN ERROR?.</remarks>
        /// <param name="method">String of method name user has previously defined</param>
        /// <param name="arguments">String array of values to be passed into the method</param>
        /// <returns>Return code expressed as a string</returns>
        string CallMethod(string method, string[] arguments);
        /// <summary>Method for parsing a block of 'code' delimited by commands "while" and "endloop" into implementation C# code<br/><br/>
        /// Parsing shuld comprise of splitting the line into the loop condition and the looped commands<br/>
        /// C# implementation should control the execution of commands (already available in <see cref="InterpretLine(string)"/> ) according to the specified condition</summary>
        /// <remarks>Returns a string dependant upon parsing success; containing either a success phrase or details about ?AN ERROR?.</remarks>
        /// <param name="whileCondition">String containing condition for execution</param>
        /// <param name="loopLines">String array containing loop logic written in parsable 'code'</param>
        /// <returns>Return code expressed as a string</returns>
        string DefineWhileLoop(string whileCondition, string[] loopLines);
    }
}