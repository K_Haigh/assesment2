﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;

namespace Programming_Learning_Environment
{
    /// <summary>Concrete class implementing Interpreter interface</summary>
    public class Interpreter : I_Interpreter
    {
        /// <summary>Object identifier for drawing implementation</summary>
        private Render output;
        /// <summary>New line character, as defined by 'code' syntax</summary>
        readonly char[] delimitLine = Factory.makeCharacterArray('\n');
        /// <summary>Seperating character between 'command' and 'data', as defined by 'code' syntax</summary>
        readonly char[] delimitCommand = Factory.makeCharacterArray(' ');
        /// <summary>Next datum character, as defined by 'code' syntax</summary>
        readonly char[] delimitData = Factory.makeCharacterArray(',');
        /// <summary>'Commands' defined by 'code' language</summary>
        readonly string[] commands = Factory.makeCommandArray();
        /// <summary>Colours available within <see cref="Color"/></summary>
        readonly string[] colours = Factory.makeColourArray();
        /// <summary>Constructor:<br/>
        /// Initialises <see cref="output"/> to the <see cref="Render"/> object that will handle drawing functionality</summary>
        /// <param name="component"><see cref="Graphics"/> context of the GUI control that will display drawing operations ot the user</param>
        public Interpreter(Graphics component)
        {
            output = Factory.makeRender(component);
        }


        /// <inheritdoc cref="I_Interpreter.CheckCommandLine(string)"/>
        public string CheckCommandLine(string lineIn)
        {
            string[] test = lineIn.Split(delimitCommand);

            // Almost all (*) commands must split into exactly two parts: command<space>data. If not, return error.
            if (test.Length != 2 && !test[0].Equals("reset") && !test[0].Equals("clear")) // *Clear & Reset are the only language defined commands that don't require data.
            {
                return "Line: " + lineIn + "\n" +
                        "This line does not follow the structure 'command' <space> 'data'\n\n" +
                        "Correct examples would be: 'moveto 100,100' OR 'setcolour red'\n" +
                        "OR 'clear', where no data is required";
            }
            else if ( !commands.Contains( test[0].Trim() ) ) // If the command is parsable, but not recognised, return error
            {
                return "Line: " + lineIn + "\n" +
                        "This line does not contain a valid command\n\n" +
                        "Valid command words are:\n" + String.Join(", ", commands);
            }
            else if (test[0].Equals("reset") || test[0].Equals("clear")) // If the command was Clear or Reset, that's fine.
            {
                return "ignore";
            }
            else if ( test[0].Equals("setcolour") && Int32.TryParse(test[1], out int success) ) // If the command was SetColour, the data cannot be an integer, return error
            {
                return  "Line: " + lineIn + "\n" +
                        "This command cannot take whole numbers as input. A worded colour must be given.\n\n" +
                        "Valid colours can be found in help documentation, or online at:\n" +
                        "https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.colors";
            }
            else // If the command is recognised and requires data (not Clear or Reset), check the data for errors.
                return CheckCommandParameters(lineIn);
        }

        /// <inheritdoc cref="I_Interpreter.CheckCommandParameters(string)"/>
        public string CheckCommandParameters(string lineIn)
        {
            // Whole input required for context, not just data
            string[] testLine = lineIn.Split(delimitCommand,2);
            string dataIn = testLine[1];
            string[] test = dataIn.Split(delimitData);

            if ( ( dataIn.Trim().Equals("on") || dataIn.Trim().Equals("off") ) && testLine[0].Equals("fill") ) // On or Off is only valid for a Fill command
            {
                return "ignore";
            }
            else if ( colours.Contains( dataIn.Trim().ToLower() ) && testLine[0].Equals("setcolour") ) // <colour> is only valid for a SetColour command
            {
                return "ignore";
            }
            else if ( colours.Contains(dataIn.Trim()) && !testLine[0].Equals("setcolour") ) // Colour data can only be used with SetColour command, otherwise invalid
            {
                return "Data: " + lineIn + "\n" +
                        "This command can only take whole numbers as arguments. Only \"setcolour\" can be used with recognised colours.";
            }
            else if (!Int32.TryParse(test[0], out int datum1)) // If the first datum cannot be parsed to an integer, the data cannot be valid. String data handled by the above if-statements
            {
                return  "Data: " + lineIn + "\n" +
                        "This is not valid data. It is not a recognised colour, or a whole number  written as an integer (1 or 2 or 10...).\n\n" +
                        "Valid colours can be found in help documentation, or online at:\n" +
                        "https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.colors";
            }
            else if (Int32.TryParse(test[0], out int datum2)) // If the first datum is parsable to an integer, the rest should be too...
            {
                foreach (string datum in test)
                {
                    if (!Int32.TryParse(datum, out int intDatum)) // ...If not, data invalid.
                    {
                        return  "Data: " + lineIn + "\n\n" +
                                "The data entered does not entirely consist of whole numbers,  written as an integer (1 or 2 or 10...).\n" +
                                "Please ensure every line only contains one type of data";
                    }
                }
                return "ignore";
            }
            else // Should not be possible to reach this point via the GUI. May be possible with unit testing. Ensures there is always a returned string.
                return  "Data: " + lineIn + "\n\n" +
                        "The data entered was not recognised as a valid data type of this command" +
                        "Please check the data type expected by the command you are tying to input " + dataIn + " into.";
        }


        /// <summary>Extension of <see cref="I_Interpreter.InterpretLine(string)"/><br/>
        /// Calls InterpretLine for each line in piece of 'code'.</summary>
        /// <param name="codeIn">Multiple lines of commands as a single string, delimited by newline characters</param>
        /// <returns></returns>
        public string InterpretCode(string codeIn)
        {
            string[] codeLines = codeIn.Split(delimitLine);
            string errorCode;

            foreach (string line in codeLines)
            {
                errorCode = InterpretLine(line); // Error checking is an integral part of InterpretLine, carrys over to InterpretCode

                if (!errorCode.Equals("ignore"))
                {
                    return errorCode;
                }
            }

            return "ignore";
        }

        /// <inheritdoc cref="I_Interpreter.InterpretLine(string)"/>
        /// <remarks><br/>Implementation uses selection statements to execute the correct handling of subsequent data, based in initial command<br/>
        /// Runtime based errors are caught using Try-catch blocks. Command and Syntax checking asertains whether the correct data is present for the given command. It does not process the data thus does not catch context based errors - such as incomplete coordinates or missing values.</remarks>
        public string InterpretLine(string lineIn)
        {
            string data;
            string[] dataPointsString;
            int[] dataPoints;
            int count;

            string instruction = lineIn.Split(delimitCommand, 2)[0];

            switch (instruction)
            {
                case "moveto":
                    data = lineIn.Split(delimitCommand, 2)[1];

                    dataPointsString = data.Split(delimitData);
                    dataPoints = Factory.makeIntegerArray(dataPointsString.Length);
                    count = 0;
                    foreach (string datum in dataPointsString)
                    {
                        dataPoints[count] = Int32.Parse(dataPointsString[count].Trim());
                        count++;
                    }

                    try // Could have provided <moveto x,_>. 'Compile-time' checking does not catch insufficiant data provided.
                    {
                        output.MoveTo(dataPoints[0], dataPoints[1]);
                        return "ignore";
                    }
                    catch (IndexOutOfRangeException)
                    {
                        return  "Data: " + lineIn + "\n" +
                                "This data is incomplete for the method 'moveto'. A coordinate must be specified consisting of two whole numbers.";
                    }

                case "drawto":
                    data = lineIn.Split(delimitCommand, 2)[1];

                    dataPointsString = data.Split(delimitData);

                    dataPoints = Factory.makeIntegerArray(dataPointsString.Length);
                    count = 0;
                    foreach (string datum in dataPointsString)
                    {
                        dataPoints[count] = Int32.Parse(datum.Trim());
                        count++;
                    }

                    try // Could have provided <moveto y,_>. 'Compile-time' checking does not catch insufficiant data provided.
                    {
                        output.DrawTo(dataPoints[0], dataPoints[1]);
                        return "ignore";
                    }
                    catch (IndexOutOfRangeException)
                    {
                        return "Data: " + lineIn + "\n" +
                        "This data is incomplete for the method 'drawto'. A coordinate must be specified consisting of two whole numbers.";
                    }

                case "setcolour":
                    data = lineIn.Split(delimitCommand, 2)[1];
                    output.SetColour( getColour(data.Trim()) );
                    return "ignore";

                case "fill":
                    data = lineIn.Split(delimitCommand, 2)[1];

                    if ( data.Trim().Equals("on") )
                    {
                        output.SetFill(true);
                    }
                    else if (data.Trim().Equals("off"))
                    {
                        output.SetFill(false);
                    }
                    return "ignore";

                case "circle":
                    data = lineIn.Split(delimitCommand, 2)[1];

                    output.Circle(Int32.Parse(data.Trim()));
                    return "ignore";

                case "triangle":
                    data = lineIn.Split(delimitCommand, 2)[1];

                    dataPointsString = data.Split(delimitData);
                    dataPoints = Factory.makeIntegerArray(dataPointsString.Length);
                    count = 0;
                    foreach (string datum in dataPointsString)
                    {
                        dataPoints[count] = Int32.Parse(dataPointsString[count].Trim());
                        count++;
                    }

                    if (dataPoints.Length == 2) // Basic triangle
                    {
                        output.Triangle(dataPoints[0], dataPoints[1]);
                        return "ignore";
                    }
                    else if (dataPoints.Length == 3) // Advanced triangle, with angle
                    {
                        output.Triangle(dataPoints[0], dataPoints[1], dataPoints[2]);
                        return "ignore";
                    }
                    else // Could have provided <triangle a,_>. 'Compile-time' checking does not catch insufficiant data provided.
                    {
                        return  "Data: " + lineIn + "\n" +
                                "This data is incomplete for the method 'triangle'. At least two whole number lengths must be specified.\n" +
                                "Alternatively, two whole number lengths and a whole number angle, measured in degrees.";
                    }

                case "rectangle":
                    data = lineIn.Split(delimitCommand, 2)[1];

                    dataPointsString = data.Split(delimitData);
                    dataPoints = Factory.makeIntegerArray(dataPointsString.Length);
                    count = 0;
                    foreach (string datum in dataPointsString)
                    {
                        dataPoints[count] = Int32.Parse(dataPointsString[count].Trim());
                        count++;
                    }

                    try // Could have provided <triangle b,_>. 'Compile-time' checking does not catch insufficiant data provided.
                    {
                    output.Rectangle(dataPoints[0], dataPoints[1]);
                        return "ignore";
                    }
                    catch (IndexOutOfRangeException)
                    {
                        return  "Data: " + lineIn + "\n" +
                                "This data is incomplete for the method 'rectangle'. Two whole number lengths must be specified.";
                    }

                case "polygon":
                    data = lineIn.Split(delimitCommand, 2)[1];

                    dataPointsString = data.Split(delimitData);
                    dataPoints = Factory.makeIntegerArray(dataPointsString.Length);
                    count = 0;
                    foreach (string datum in dataPointsString)
                    {
                        dataPoints[count] = Int32.Parse(dataPointsString[count].Trim());
                        count++;
                    }

                    List<KeyValuePair<int, int>> listVertices = Factory.makePointsList();
                    count = 1;

                    while (count < dataPoints.Length) 
                    {
                        try // Could have provided <polygon 3,100,100,200,100,150,150,200,_>. 'Compile-time' checking does not catch insufficiant data provided.
                        {
                            listVertices.Add( Factory.makeCoordinate( dataPoints[count], dataPoints[count + 1] ) );
                            count += 2;
                        }
                        catch (IndexOutOfRangeException)
                        {
                            /*
                             *  Force loop to exit without correction.
                             *  With compulsary syntax checking, this error can only ever occur when a user enters an incomplete coordinate.
                             *      i.e 'polygon 4,100,100,100,200,200,150,100      7 ordinates, 3.5 pairs of coordinates.
                             *      
                             *  Where this occurs, this is a user logic error. Not a syntactic error to be handled.
                             *  The broken/incomplete coordinate is not accepted.
                            */
                            count += 2;
                        }
                    }

                    int countVertices = listVertices.Count;

                    if (countVertices < 2) // Polygon must have at least two points
                    {
                        return  "Data: " + lineIn + "\n" +
                                "This data is incomplete for the method 'polygon'.\n" +
                                "At least five whole numbers must be given - a number of vertices and two coordinates, each a pair of whole numbers.\n\n" +
                                "Every additional vertex (shape corner) must be given as an additional pair of whole numbers.";
                    }
                    else if ( dataPoints[0] != countVertices) // Number of vertices stated and number of points given could differ. 'Compile-time' checking does not catch data mis-match.
                    {
                        return "Data: " + lineIn + "\n" +
                                "This data is incorrect for the described 'polygon'.\n\n" +
                                "The number of sides stated is " + dataPoints[0] +"\n" +
                                "The number of coordinated given is " + listVertices.Count;
                    }
                    else
                    {
                        output.Polygon(dataPoints[0], listVertices);
                        return "ignore";
                    }

                case "clear":
                    output.Clear();
                    return "ignore";

                case "reset":
                    output.Reset();
                    return "ignore";

                // Should not be possible to reach this point via the GUI. May be possible with unit testing. Ensures there is always a returned string.
                default:
                    return  "Code line: " + lineIn + "\n" +
                            "This line of code is completely unrecognised. Congratulations, this output should never be seen!";
            }
        }

        /// <summary>Method to return a correctly formatted (CamelCaps) string representing a colour, given all-lowercase input of an otherwise valid colour name</summary>
        /// <remarks>Required due to a quirk of string case handling. 'Code' syntax calls for case insensitivity, therfore cast all strings to lower case.<br/>
        /// Colors cast from strings using <see cref="Color.FromName(string)"/> requires a CamelCaps string. This there is a need for some function which maps lowercase user input colours to system Colors.</remarks>
        /// <param name="colourIn">Lowercase string of a valid <see cref="Color"/></param>
        /// <returns><see cref="Color"/> object corresponding to verbose input</returns>
        private Color getColour(string colourIn)
        {
            switch (colourIn)
            {
                case "aliceblue":
                    return Color.FromName("AliceBlue");

                case "antiquewhite":
                    return Color.FromName("AntiqueWhite");

                case "aqua":
                    return Color.FromName("Aqua");

                case "aquamarine":
                    return Color.FromName("Aquamarine");

                case "azure":
                    return Color.FromName("Azure");

                case "beige":
                    return Color.FromName("Beige");

                case "bisque":
                    return Color.FromName("Bisque");

                case "black":
                    return Color.FromName("Black");

                case "blanchedalmond":
                    return Color.FromName("BlanchedAlmond");

                case "blue":
                    return Color.FromName("blue");

                case "blueviolet":
                    return Color.FromName("BlueViolet");

                case "brown":
                    return Color.FromName("Brown");

                case "burlyWood":
                    return Color.FromName("BurlyWood");

                case "cadetblue":
                    return Color.FromName("CadetBlue");

                case "chartreuse":
                    return Color.FromName("Chartreuse");

                case "chocolate":
                    return Color.FromName("Chocolate");

                case "coral":
                    return Color.FromName("Coral");

                case "cornflowerblue":
                    return Color.FromName("CornflowerBlue");

                case "cornsilk":
                    return Color.FromName("Cornsilk");

                case "crimson":
                    return Color.FromName("Crimson");

                case "cyan":
                    return Color.FromName("Cyan");

                case "darkblue":
                    return Color.FromName("DarkBlue");

                case "darkcyan":
                    return Color.FromName("DarkCyan");

                case "darkgoldenrod":
                    return Color.FromName("DarkGoldenrod");

                case "darkgray":
                    return Color.FromName("DarkGray");

                case "darkgreen":
                    return Color.FromName("DarkGreen");

                case "darkkhaki":
                    return Color.FromName("DarkKhaki");

                case "darkmagenta":
                    return Color.FromName("DarkMagenta");

                case "darkolivegreen":
                    return Color.FromName("DarkOliveGreen");

                case "darkorange":
                    return Color.FromName("DarkOrange");

                case "darkorchid":
                    return Color.FromName("DarkOrchid");

                case "darkred":
                    return Color.FromName("DarkRed");

                case "darksalmon":
                    return Color.FromName("DarkSalmon");

                case "darkseagreen":
                    return Color.FromName("DarkSeaGreen");

                case "darkslateblue":
                    return Color.FromName("DarkSlateBlue");

                case "darkslategray":
                    return Color.FromName("DarkSlateGray");

                case "darkturquoise":
                    return Color.FromName("DarkTurquoise");

                case "darkviolet":
                    return Color.FromName("DarkViolet");

                case "deeppink":
                    return Color.FromName("DeepPink");

                case "deepskyblue":
                    return Color.FromName("DeepSkyBlue");

                case "dimgray":
                    return Color.FromName("DimGray");

                case "dodgerblue":
                    return Color.FromName("DodgerBlue");

                case "firebrick":
                    return Color.FromName("Firebrick");

                case "floralwhite":
                    return Color.FromName("FloralWhite");

                case "forestgreen":
                    return Color.FromName("ForestGreen");

                case "fuchsia":
                    return Color.FromName("Fuchsia");

                case "gainsboro":
                    return Color.FromName("Gainsboro");

                case "ghostwhite":
                    return Color.FromName("GhostWhite");

                case "gold":
                    return Color.FromName("Gold");

                case "goldenrod":
                    return Color.FromName("Goldenrod");

                case "gray":
                    return Color.FromName("Gray");

                case "green":
                    return Color.FromName("Green");

                case "greenyellow":
                    return Color.FromName("GreenYellow");

                case "honeydew":
                    return Color.FromName("Honeydew");

                case "hotpink":
                    return Color.FromName("HotPink");

                case "indianred":
                    return Color.FromName("IndianRed");

                case "indigo":
                    return Color.FromName("Indigo");

                case "ivory":
                    return Color.FromName("Ivory");

                case "khaki":
                    return Color.FromName("Khaki");

                case "lavender":
                    return Color.FromName("Lavender");

                case "lavenderblush":
                    return Color.FromName("LavenderBlush");

                case "lawngreen":
                    return Color.FromName("LawnGreen");

                case "lemonchiffon":
                    return Color.FromName("LemonChiffon");

                case "lightblue":
                    return Color.FromName("LightBlue");

                case "lightcoral":
                    return Color.FromName("LightCoral");

                case "lightcyan":
                    return Color.FromName("LightCyan");

                case "lightgoldenrodyellow":
                    return Color.FromName("LightGoldenrodYellow");

                case "lightgray":
                    return Color.FromName("LightGray");

                case "lightgreen":
                    return Color.FromName("LightGreen");

                case "lightpink":
                    return Color.FromName("LightPink");

                case "lightsalmon":
                    return Color.FromName("LightSalmon");

                case "lightseaGreen":
                    return Color.FromName("LightSeaGreen");

                case "lightskyblue":
                    return Color.FromName("LightSkyBlue");

                case "lightslategray":
                    return Color.FromName("LightSlateGray");

                case "lightsteelblue":
                    return Color.FromName("LightSteelBlue");

                case "lightyellow":
                    return Color.FromName("LightYellow");

                case "lime":
                    return Color.FromName("Lime");

                case "limegreen":
                    return Color.FromName("LimeGreen");
                
                case "linen":
                    return Color.FromName("Linen");

                case "magenta":
                    return Color.FromName("Magenta");

                case "maroon":
                    return Color.FromName("Maroon");

                case "mediumaquamarine":
                    return Color.FromName("MediumAquamarine");

                case "mediumblue":
                    return Color.FromName("MediumBlue");

                case "mediumorchid":
                    return Color.FromName("MediumOrchid");

                case "mediumpurple":
                    return Color.FromName("MediumPurple");

                case "mediumseagreen":
                    return Color.FromName("MediumSeaGreen");

                case "mediumslateblue":
                    return Color.FromName("MediumSlateBlue");

                case "mediumspringgreen":
                    return Color.FromName("MediumSpringGreen");

                case "mediumturquoise":
                    return Color.FromName("MediumTurquoise");

                case "mediumvioletred":
                    return Color.FromName("MediumVioletRed");

                case "midnightblue":
                    return Color.FromName("MidnightBlue");

                case "mintcream":
                    return Color.FromName("MintCream");

                case "mistyrose":
                    return Color.FromName("MistyRose");

                case "moccasin":
                    return Color.FromName("Moccasin");

                case "navajowhite":
                    return Color.FromName("NavajoWhite");

                case "navy":
                    return Color.FromName("Navy");

                case "oldlace":
                    return Color.FromName("OldLace");

                case "olive":
                    return Color.FromName("Olive");

                case "olivedrab":
                    return Color.FromName("OliveDrab");

                case "orange":
                    return Color.FromName("Orange");

                case "orangeRed":
                    return Color.FromName("OrangeRed");

                case "orchid":
                    return Color.FromName("Orchid");

                case "palegoldenrod":
                    return Color.FromName("PaleGoldenrod");

                case "palegreen":
                    return Color.FromName("PaleGreen");

                case "paleturquoise":
                    return Color.FromName("PaleTurquoise");

                case "palevioletRed":
                    return Color.FromName("PaleVioletRed");

                case "papayawhip":
                    return Color.FromName("PapayaWhip");

                case "peachpuff":
                    return Color.FromName("PeachPuff");

                case "peru":
                    return Color.FromName("Peru");

                case "pink":
                    return Color.FromName("Pink");

                case "plum":
                    return Color.FromName("Plum");

                case "powderblue":
                    return Color.FromName("PowderBlue");

                case "purple":
                    return Color.FromName("Purple");

                case "red":
                    return Color.FromName("Red");

                case "rosybrown":
                    return Color.FromName("RosyBrown");

                case "royalblue":
                    return Color.FromName("RoyalBlue");

                case "saddlebrown":
                    return Color.FromName("SaddleBrown");

                case "salmon":
                    return Color.FromName("Salmon");

                case "sandybrown":
                    return Color.FromName("SandyBrown");

                case "seagreen":
                    return Color.FromName("SeaGreen");

                case "seashell":
                    return Color.FromName("SeaShell");

                case "sienna":
                    return Color.FromName("Sienna");

                case "silver":
                    return Color.FromName("Silver");

                case "skyblue":
                    return Color.FromName("SkyBlue");

                case "slateblue":
                    return Color.FromName("SlateBlue");

                case "slategray":
                    return Color.FromName("SlateGray");

                case "snow":
                    return Color.FromName("Snow");

                case "springgreen":
                    return Color.FromName("SpringGreen");

                case "steelblue":
                    return Color.FromName("SteelBlue");

                case "tan":
                    return Color.FromName("Tan");

                case "teal":
                    return Color.FromName("Teal");

                case "thistle":
                    return Color.FromName("Thistle");

                case "tomato":
                    return Color.FromName("Tomato");

                case "transparent":
                    return Color.FromName("Transparent");

                case "turquoise":
                    return Color.FromName("Turquoise");

                case "violet":
                    return Color.FromName("Violet");

                case "wheat":
                    return Color.FromName("Wheat");

                case "white":
                    return Color.FromName("White");

                case "whitesmoke":
                    return Color.FromName("WhiteSmoke");

                case "yellow":
                    return Color.FromName("Yellow");

                case "yellowgreen":
                    return Color.FromName("YellowGreen");

                //No colours any more, I want them painted black.
                default:
                    return Color.FromName("Black");

            }
        }



    // Implement in Part 2
        /// <inheritdoc cref="I_Interpreter.DeclareVariable(string, int)"/>
        public string DeclareVariable(string variableName, int value)
        {
            throw Factory.makeToDo();
        }

        /// <inheritdoc cref="I_Interpreter.DeclareVariable(string, string)"/>
        public string DeclareVariable(string variableName, string value)
        {
            throw Factory.makeToDo();
        }

        /// <inheritdoc cref="I_Interpreter.DeclareMethod(string, string[], string[])"/>
        public string DeclareMethod(string methodName, string[] methodArguments, string[] methodLines)
        {
            throw Factory.makeToDo();
        }

        /// <inheritdoc cref="I_Interpreter.CallMethod(string, string[])"/>
        public string CallMethod(string method, string[] arguments)
        {
            throw Factory.makeToDo();
        }

        /// <inheritdoc cref="I_Interpreter.DefineWhileLoop(string, string[])"/>
        public string DefineWhileLoop(string whileCondition, string[] loopLines)
        {
            throw Factory.makeToDo();
        }
    }
}
