﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programming_Learning_Environment
{
    //There shouldn't be a single <new> keyword anywhere else in the project
    static class Factory
    {
        // List of raw materials
        /*
        * Programming_Window programmingWindow;
        * Render render;
        * Interpreter interpreter;
        * SaveFileDialog save;
        * OpenFileDialog open;
        * Pen pen;
        * Point point;
        * NotImplementedException toDo;
        * KeyValuePair<int, int> coordinate
        * List<KeyValuePair<int, int>> coordinateList;
        * int[] intArray;
        * char[] charArray;
        * string[] stringArray;
        */

        // List of manufacturing methods
        public static Programming_Window makeProgrammingWindow()
        {
            return new Programming_Window();
        }
        public static Render makeRender(Graphics g)
        {
            return new Render(g);
        }
        public static Interpreter makeInterpreter(Graphics g)
        {
            return new Interpreter(g);
        }
        public static SaveFileDialog makeSaveFileDialog()
        {
            return new SaveFileDialog();
        }
        public static OpenFileDialog makeOpenFileDialog()
        {
            return new OpenFileDialog();
        }
        public static Pen makeBlackPen()
        {
            return new Pen(Color.Black);
        }
        public static Point makePoint(int x, int y)
        {
            return new Point(x, y);
        }
        public static Point[] makePointArray(int size)
        {
            return new Point[size];
        }
        public static NotImplementedException makeToDo()
        {
            return new NotImplementedException();
        }
        public static KeyValuePair<int, int> makeCoordinate(int x, int y)
        {
            return new KeyValuePair<int, int>(x, y);
        }
        public static List<KeyValuePair<int, int>> makePointsList()
        {
            return new List<KeyValuePair<int, int>>();
        }
        public static int[] makeIntegerArray(int size)
        {
            return new int[size];
        }
        public static char[] makeCharacterArray(int size)
        {
            return new char[size];
        }
        public static string[] makeStringArray(int size)
        {
            return new string[size];
        }

    // Specific data structure methods
        public static char[] makeCharacterArray(char character)
        {
            return new char[] { character };
        }
        public static string[] makeCommandArray()
        {
            return new string[] { "moveto", "drawto", "setcolour", "fill", "circle", "triangle", "rectangle", "polygon", "clear", "reset" };
        }
        public static string[] makeColourArray()
        {
            return new string[] { "aliceblue", "antiquewhite", "aqua", "aquamarine", "azure", "beige", "bisque", "black", "blanchedalmond", "blue", "blueviolet", "brown", "burlyWood", "cadetblue", "chartreuse", "chocolate", "coral", "cornflowerblue", "cornsilk", "crimson", "cyan", "darkblue", "darkcyan", "darkgoldenrod", "darkgray", "darkgreen", "darkkhaki", "darkmagenta", "darkolivegreen", "darkorange", "darkorchid", "darkred", "darksalmon", "darkseagreen", "darkslateblue", "darkslategray", "darkturquoise", "darkviolet", "deeppink", "deepskyblue", "dimgray", "dodgerblue", "firebrick", "floralwhite", "forestgreen", "fuchsia", "gainsboro", "ghostwhite", "gold", "goldenrod", "gray", "green", "greenyellow", "honeydew", "hotpink", "indianred", "indigo", "ivory", "khaki", "lavender", "lavenderblush", "lawngreen", "lemonchiffon", "lightblue", "lightcoral", "lightcyan", "lightgoldenrodyellow", "lightgray", "lightgreen", "lightpink", "lightsalmon", "lightseaGreen", "lightskyblue", "lightslategray", "lightsteelblue", "lightyellow", "lime", "limegreen", "linen", "magenta", "maroon", "mediumaquamarine", "mediumblue", "mediumorchid", "mediumpurple", "mediumseagreen", "mediumslateblue", "mediumspringgreen", "mediumturquoise", "mediumvioletred", "midnightblue", "mintcream", "mistyrose", "moccasin", "navajowhite", "navy", "oldlace", "olive", "olivedrab", "orange", "orangeRed", "orchid", "palegoldenrod", "palegreen", "paleturquoise", "palevioletRed", "papayawhip", "peachpuff", "peru", "pink", "plum", "powderblue", "purple", "red", "rosybrown", "royalblue", "saddlebrown", "salmon", "sandybrown", "seagreen", "seashell", "sienna", "silver", "skyblue", "slateblue", "slategray", "snow", "springgreen", "steelblue", "tan", "teal", "thistle", "tomato", "transparent", "turquoise", "violet", "wheat", "white", "whitesmoke", "yellow", "yellowgreen" };
        }
}
}
