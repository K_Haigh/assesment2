﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programming_Learning_Environment
{

    class Draw : I_Draw
    {
        //Instance variables
        private Graphics canvass;
        private Pen pen;
        private int x_ord, y_ord;
        private Boolean filling;

        public Draw (Graphics component)
        {
            canvass = component;
            x_ord = 0;
            y_ord = 0;
            filling = false;

            Clear();
        }

        public void Circle(int radius)
        {
            //Debugging
            Console.WriteLine("Fill shape really is: " + filling);

            if (! filling)
            {
                canvass.DrawEllipse(pen, x_ord, y_ord, radius, radius);
            }
            if (filling)
            {
                canvass.FillEllipse(pen.Brush, x_ord, y_ord, radius, radius);
            }

        }

        public void Clear()
        {
            canvass.Clear(Color.White);
        }

        public void DrawTo(int x_ord, int y_ord)
        {
            Console.WriteLine("4) " + pen);
            Console.WriteLine("5) " + getPen() );

            canvass.DrawLine(pen, this.x_ord, this.y_ord, x_ord, y_ord);
            MoveTo(x_ord, y_ord);
        }

        public void MoveTo(int x_ord, int y_ord)
        {
            this.x_ord = x_ord;
            this.y_ord = y_ord;
        }

        public void Polygon(int points, List<KeyValuePair<int, int>> coordinates)
        {
            //int corners = coordinates.Count;

            //Debugging
            Console.WriteLine("Fill shape really is: " + filling);

            int count = 0;
            Point[] vertices = new Point[points];
            
            foreach (KeyValuePair<int, int> vertex in coordinates)
            {
                vertices[count] = new Point (coordinates[count].Key, coordinates[count].Value);
                count++;
            }

            if (!filling)
            {
                canvass.DrawPolygon(pen, vertices);
            }
            if (filling)
            {
                canvass.FillPolygon(pen.Brush, vertices);
            }
        }

        public void Rectangle(int a_side, int b_side)
        {
            //Debugging
            Console.WriteLine("Fill shape really is: " + filling);

            if (!filling)
            {
                canvass.DrawRectangle(pen, x_ord, y_ord, a_side, b_side);
            }
            if (filling)
            {
                canvass.FillRectangle(pen.Brush, x_ord, y_ord, a_side, b_side);
            }
        }

        public void Reset()
        {
            Clear();
            x_ord = 0;
            y_ord = 0;
        }

        public void SetColour(Color colour)
        {
            pen = new Pen(colour);
            filling = false;
        }

        public Pen getPen()
        {
            return pen;
        }

        public void SetFill(bool toFill)
        {
            filling = toFill;
        }

        
        //Right-angled triangle, width and height
        public void Triangle(int width, int height)
        {
            Point[] abc = new Point[3];
            abc[0] = new Point(x_ord, y_ord);
            abc[1] = new Point(x_ord + width, y_ord);
            abc[2] = new Point(x_ord, y_ord + height);

            if (!filling)
            {
                canvass.DrawPolygon(pen, abc);
            }
            if (filling)
            {
                canvass.FillPolygon(pen.Brush, abc);
            }
        }
        //Triangle with a horizontal base, two side lengths and an interior angle
        public void Triangle(int a_side, int b_side, int angle)
        {
            //Debugging
            Console.WriteLine("Fill shape really is: " + filling);

            Point[] abc = new Point[3];
            abc[0] = new Point(x_ord, y_ord);
            abc[1] = new Point(x_ord + a_side, y_ord);
            double radianAngle = 2*Math.PI*(angle/360.0);

            int cX = (int)Math.Round(x_ord + b_side * Math.Cos(radianAngle));
            int cY = (int)Math.Round(y_ord + b_side * Math.Sin(radianAngle));
            abc[2] = new Point(cX, cY);

            if (!filling)
            {
                canvass.DrawPolygon(pen, abc);
            }
            if (filling)
            {
                canvass.FillPolygon(pen.Brush, abc);
            }
        }
    }
}
