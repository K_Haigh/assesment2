﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Programming_Learning_Environment;
using System.Windows.Forms;
using System.Drawing;

namespace Test_PLE
{
    //Class providing method design requirements for further functionality
    [TestClass]
    public class InterpreterDevelopmentTesting
    {
        [TestMethod]
        public void Develop_DeclareIntegerVariable()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define variable
            Interpreter testInterpreter = new Interpreter(testGraphics);
            string testName = "TheUltimateQuestion";
            int testValue = 42;

            //Run - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.DeclareVariable(testName, testValue), "ignore", "Variable declaration has returned an error, handling code is erronious.");
        }

        [TestMethod]
        public void Develop_DeclareStringVariable()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define variable
            Interpreter testInterpreter = new Interpreter(testGraphics);
            string testName = "TheUltimateQuestion";
            string testValue = "Forty-two";

            //Run - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.DeclareVariable(testName, testValue), "ignore", "Variable declaration has returned an error, handling code is erronious.");
        }

        [TestMethod]
        public void Develop_DeclareMethod() // Independant of the implemetnation of 'variables', therefore not method parameters tested yet.
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define method
            Interpreter testInterpreter = new Interpreter(testGraphics);
            string testMethodName = "TheUltimateAnswer";
            string[] testMethodParameters = new string[] { "" };
            string[] testMethodLines = new string[] { "setcolour red", "rectangle 42,42", "setcolour green", "fill on", "circle 42", "setcolour blue", "fill off", "triangle 42,42,42" };

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.DeclareMethod(testMethodName,testMethodParameters, testMethodLines ), "ignore", "Method declaration has returned an error, handling code is erronious.");
        }

        [TestMethod]
        public void Develop_WhileLoop()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define loop
            Interpreter testInterpreter = new Interpreter(testGraphics);
            string testLoopReferance = "WhileLoop1";
            string[] testLoopLines = new string[] { "while count<10", "circle radius", "radius = radius+10", "count = count+1" };

            //'Run' - 
            Assert.AreEqual(testInterpreter.DeclareVariable("count", 0), "ignore", "Variable declaration has returned an error, handling code is erronious.");
            Assert.AreEqual(testInterpreter.DeclareVariable("radius", 20), "ignore", "Variable declaration has returned an error, handling code is erronious.");

            Assert.AreEqual(testInterpreter.DefineWhileLoop(testLoopReferance, testLoopLines), "ignore", "Variable declaration has returned an error, handling code is erronious.");
        }
    }
}
