﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Test_PLE
{
    //Class testing 'Programming_Window' class for all base/part 1 user interface functionality
    [TestClass]
    public class WindowTesting
    {
        /*
         * All aspects of class are visual. 
         * No methods with return types.
         * No set/get-able properties.
         */
    }
}
